# EasyPopup
[![](https://jitpack.io/v/com.gitlab.joielechong/EasyPopup.svg)](https://jitpack.io/#com.gitlab.joielechong/EasyPopup)

### PopupWindow

Encapsulation of PopupWindow makes it easier, faster, and faster to use in projects

### Project Features

- Chained calls: In addition to the traditional PopupWindow usage method, more methods have been added
- Pop-up PopupWindow is easier and simpler with pop-up methods relative to AnchorView
- Supports PopupWindow when the background is dark, the specified ViewGroup background is darkened, the darkened color is set, etc. (API>=18)
- Added a simple lifecycle approach, customizing PopupWindow, processing logic more convenient and clearer

### Sample

![EasyPopup](./images/easy_popup.gif)

### Usage

Step 1. Add it in your root build.gradle at the end of repositories:

```gradle
allprojects {
    repositories {
	//...
	maven { url 'https://jitpack.io' }
    }
}
```

Step 2. Add the dependency

```gradle
dependencies {
    implementation 'com.gitlab.joielechong:easypopup:1.1.3@aar'
}
```

[heck for Newest Version](https://gitlab.com/joielechong/EasyPopup/tags)

### Usage

#### 1. Basic use

**Create EasyPopup object**

You can call the setXxx() method to set the property, and finally call the createPopup() method to initialize the PopupWindow.

```java
private EasyPopup mCirclePop;
mCirclePop = EasyPopup.create()
        .setContentView(this, R.layout.layout_circle_comment)
        .setAnimationStyle(R.style.RightPopAnim)
        // Allow click outside PopupWindow to make it disappear
        .setFocusAndOutsideEnable(true)
        .apply();
```

**Initialize View**

You can call the findViewById() method to get a View object.

```java
TextView tvZan=mCirclePop.findViewById(R.id.tv_zan);
TextView tvComment=mCirclePop.findViewById(R.id.tv_comment);
tvZan.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        ToastUtils.showShort("Thumbs up");
        mCirclePop.dismiss();
    }
});

tvComment.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        ToastUtils.showShort("comment");
        mCirclePop.dismiss();
    }
});
```

**Display**

Display related to the view position

```java
/**
 * Relative anchor view display，applicable width and height are not match_parent
 *
 * @param anchor
 * @param yGravity  alignment in the vertical direction
 * @param xGravity  Horizontal alignment
 * @param x            horizontal offset
 * @param y            vertical offset
 */
mCirclePop.showAtAnchorView(view, YGravity.CENTER, XGravity.LEFT, 0, 0);
```

In addition to the showAtAnchorView() method, the showAsDropDown() and showAtLocation() methods are retained internally.

**Note: If you use YGravity and XGravity, make sure that PopupWindow does not exceed the screen boundary after use. If you exceed the screen boundary, YGravity and XGravity may not work, and you will not achieve the effect you want. **[#4](https://github.com/zyyoona7/EasyPopup/issues/4)

**Position annotations introduction**

Align vertically: YGravity

```java
YGravity.CENTER, / / vertical centered
YGravity.ABOVE, //anchor view
YGravity.BELOW, //anchor view
YGravity.ALIGN_TOP, / / aligned with the top of the anchor view
YGravity.ALIGN_BOTTOM, //anchor view bottom alignment
```

Alignment in the horizontal direction: XGravity

```java
XGravity.CENTER, / / horizontal centered
XGravity.LEFT, //anchor view left
XGravity.RIGHT, //anchor view right
XGravity.ALIGN_LEFT, / / aligned with the left side of the anchor view
XGravity.ALIGN_RIGHT, / / aligned with the right side of the anchor view
```

#### 2. Pop up PopupWindow with background darken

```java
mCirclePop = EasyPopup.create()
        .setContentView(this, R.layout.layout_circle_comment)
        .setAnimationStyle(R.style.RightPopAnim)
        // Allow click outside PopupWindow to make it disappear
        .setFocusAndOutsideEnable(true)
        // Allow the background to darken
        .setBackgroundDimEnable(true)
        // Darkening transparency (0-1), 0 is completely transparent
        .setDimValue(0.4f)
        // darkened background color
        .setDimColor(Color.YELLOW)
        // Specify any ViewGroup background to darken
        .setDimView(viewGroup)
        .apply();
```

Note: The background dimming effect only supports versions above 4.2.

#### 3.  Click outside the PopupWindow to prevent it from disappearing

```java
mCirclePop = EasyPopup.create()
        .setContentView(this, R.layout.layout_circle_comment)
        .setAnimationStyle(R.style.RightPopAnim)
        // Is it allowed to click outside the PopupWindow to disappear?
        // Set to false beyond the click will not disappear, but will respond to the return button event
        .setFocusAndOutsideEnable(false)
        .apply();
```

#### 4. Customizing PopupWindow

Three life cycles have been customized in EasyPopup:

- onPopupWindowCreated(): Called after the PopupWindow object is initialized
- onPopupWindowViewCreated(View contentView):PopupWindow is called after setting contentView and width and height
- onPopupWindowDismiss(): called when PopupWindow dismiss

Customizing PopupWindow inherits the BasePopup abstract class and implements two internal abstract methods:

- initAttributes(): The properties required by PopupWindow can be set in this method, which is called in onPopupWindowCreated()
- initViews(): initializes the view in this method, which is called in onPopupWindowViewCreated(View contentView)

**Example**

```java
public class ComplexPopup extends BasePopup<ComplexPopup> {
    private static final String TAG = "ComplexPopup";

    private Button mOkBtn;
    private Button mCancelBtn;

    public static ComplexPopup create(Context context){
        return new ComplexPopup(context);
    }

    protected ComplexPopup(Context context) {
        setContext(context);
    }


    @Override
    protected void initAttributes() {
        setContentView(R.layout.layout_complex, ViewGroup.LayoutParams.MATCH_PARENT, SizeUtils.dp2px(300));
        setFocusAndOutsideEnable(false)
            .setBackgroundDimEnable(true)
            .setDimValue(0.5f);
            //setXxx() method
    }

    @Override
    protected void initViews(View view) {
        mOkBtn = findViewById(R.id.btn_ok);
        mCancelBtn = findViewById(R.id.btn_cancel);

        mOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
```

```java
mComplexPopup = ComplexPopup.create(this)
                            .setDimView(mComplexBgDimView)
                            .createPopup();
```

#### 5. Documentation of other methods

| Method Name                              | Function                    | Remarks        |
| :--------------------------------------- | --------------------- | --------- |
| setContentView(View contentView)         | set contentView        |           |
| setContentView(Context context, @LayoutRes int layoutId) | set contentView        |           |
| setWidth(int width)                      | set width                   |           |
| setHeight(int height)                    | set high                   |           |
| setAnchorView(View view)                 | set target view             |           |
| setYGravity(@YGravity int yGravity)      | set vertical alignment              |           |
| setXGravity(@XGravity int xGravity)      | set horizontal alignment              |           |
| setOffsetX(int offsetX)                  | set horizontal offset                |           |
| setOffsetY(int offsetY)                  | set vertical                  |           |
| setAnimationStyle(@StyleRes int animationStyle) | set animation style                |           |
| getContentView()                         | get the view loaded in PopupWindow | @Nullable |
| getContext()                             | get context             | @Nullable |
| getPopupWindow()                         | get PopupWindow object       | @Nullable |
| dismiss()                                | dismiss                    |           |

#### 6. Version migration

In the latest version 1.1.0, the code structure was jumped, and the inheritance of generics was optimized on the basis of the previous ones, which made the chained calls smoother. In addition, the inheritance of EasyPopup was optimized. The naming has also been adjusted.

**i. Inherit using modification, naming modification**

- Customize PopupWindow from the original inheritance **BaseCustomPopup** to inherit **BasePopup<T>** (see demo for specific usage).
- Rename the original createPopup() method to the apply() method. The apply() method in the new version is not mandatory and will be checked in the showXxx() method. If you forget to call the apply() method, it will be called once.
- Renamed the original VerticalGravity, HorizontalGravity annotations to YGravity, and XGravity to a lot.
- Rename the original getView() method to findViewById().

**ii. Other usage adjustments**

- Whether it's customizing PopupWindow or calling EasyPopup, it's not mandatory to pass in a Context object in the constructor, because the Context object is only needed if layoutRes is passed in when setting the contentView. If you set the layout in the above way, you need to manually set the Context object: setContext(Context context)/setContentView(Context context, @LayoutRes int layoutId) method.
- Directly using EasyPopup provides a static method create()/create(Context context) method to create an object, which is cool to use.
- Added more methods, welcome to read the source code.

### TODO


### Thanks

**[RelativePopupWindow](https://github.com/kakajika/RelativePopupWindow)**<br>
**[CustomPopwindow](https://github.com/pinguo-zhouwei/CustomPopwindow)**<br>
**[android-simple-tooltip](https://github.com/douglasjunior/android-simple-tooltip)**<br>
**[EasyDialog](https://github.com/tianzhijiexian/EasyDialog/tree/master/lib)**<br>
**[Android弹窗_PopupWindow详解](http://liangjingkanji.coding.me/2017/02/11/PopupWindow/)**<br>


### License

[Copyright 2017 zyyoona7](LICENSE)
