package com.rilixtech.easypopup;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.FloatRange;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.annotation.StyleRes;
import android.support.v4.widget.PopupWindowCompat;
import android.transition.Transition;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.PopupWindow;

/**
 * Created by zyyoona7 on 2017/8/3.
 */

public abstract class BasePopup<T extends BasePopup> implements PopupWindow.OnDismissListener {
  private static final String TAG = "EasyPopup";

  private static final float DEFAULT_DIM = 0.7f;

  private PopupWindow mPopupWindow;
  private Context mContext;
  private View mContentView;
  private int mLayoutId;
  private boolean mFocusable = true;
  private boolean mOutsideTouchable = true;

  private int mWidth = ViewGroup.LayoutParams.WRAP_CONTENT;
  private int mHeight = ViewGroup.LayoutParams.WRAP_CONTENT;

  private int mAnimationStyle;

  private PopupWindow.OnDismissListener mOnDismissListener;

  //Whether the background is dark when pop pops up
  private boolean isBackgroundDim;

  //Transparency when the background is darkened
  private float mDimValue = DEFAULT_DIM;
  //Background darkening color
  @ColorInt private int mDimColor = Color.BLACK;

  //Background darkened view
  @NonNull private ViewGroup mDimView;

  private Transition mEnterTransition;
  private Transition mExitTransition;

  private boolean mFocusAndOutsideEnable = true;

  private View mAnchorView;
  @YGravity private int mYGravity = YGravity.BELOW;
  @XGravity private int mXGravity = XGravity.LEFT;
  private int mOffsetX;
  private int mOffsetY;

  private int mInputMethodMode = PopupWindow.INPUT_METHOD_FROM_FOCUSABLE;
  private int mSoftInputMode = WindowManager.LayoutParams.SOFT_INPUT_STATE_UNCHANGED;

  //Whether to re-measure the width and height
  private boolean isNeedReMeasureWH = false;
  //Is the true width and height ready?
  private boolean isRealWHAlready = false;
  private boolean isAtAnchorViewMethod = false;

  private OnRealWHAlreadyListener mOnRealWHAlreadyListener;

  protected T self() {
    //noinspection unchecked
    return (T) this;
  }

  public T apply() {
    if (mPopupWindow == null) {
      mPopupWindow = new PopupWindow();
    }

    onPopupWindowCreated();

    initContentViewAndWH();

    onPopupWindowViewCreated(mContentView);

    if (mAnimationStyle != 0) {
      mPopupWindow.setAnimationStyle(mAnimationStyle);
    }

    initFocusAndBack();
    mPopupWindow.setOnDismissListener(this);

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      if (mEnterTransition != null) {
        mPopupWindow.setEnterTransition(mEnterTransition);
      }

      if (mExitTransition != null) {
        mPopupWindow.setExitTransition(mExitTransition);
      }
    }

    return self();
  }

  private void initContentViewAndWH() {
    if (mContentView == null) {
      if (mLayoutId != 0 && mContext != null) {
        mContentView = LayoutInflater.from(mContext).inflate(mLayoutId, null);
      } else {
        throw new IllegalArgumentException(
            "The content view is null,the layoutId=" + mLayoutId + ",context=" + mContext);
      }
    }
    mPopupWindow.setContentView(mContentView);

    if (mWidth > 0
        || mWidth == ViewGroup.LayoutParams.WRAP_CONTENT
        || mWidth == ViewGroup.LayoutParams.MATCH_PARENT) {
      mPopupWindow.setWidth(mWidth);
    } else {
      mPopupWindow.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    if (mHeight > 0
        || mHeight == ViewGroup.LayoutParams.WRAP_CONTENT
        || mHeight == ViewGroup.LayoutParams.MATCH_PARENT) {
      mPopupWindow.setHeight(mHeight);
    } else {
      mPopupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
    }
    // Measure the size of the contentView
    // may not be allowed
    measureContentView();
    //Get the exact size of the contentView)
    registerOnGlobalLayoutListener();

    mPopupWindow.setInputMethodMode(mInputMethodMode);
    mPopupWindow.setSoftInputMode(mSoftInputMode);
  }

  private void initFocusAndBack() {
    if (!mFocusAndOutsideEnable) {
      //from https://github.com/pinguo-zhouwei/CustomPopwindow
      mPopupWindow.setFocusable(true);
      mPopupWindow.setOutsideTouchable(false);
      mPopupWindow.setBackgroundDrawable(null);
      //Note that the following three are contentView not PopupWindow, responding to return button events
      mPopupWindow.getContentView().setFocusable(true);
      mPopupWindow.getContentView().setFocusableInTouchMode(true);
      mPopupWindow.getContentView().setOnKeyListener(new View.OnKeyListener() {
        @Override public boolean onKey(View v, int keyCode, KeyEvent event) {
          if (keyCode == KeyEvent.KEYCODE_BACK) {
            mPopupWindow.dismiss();

            return true;
          }
          return false;
        }
      });
      //On Android 6.0 and above, it can only be solved by intercepting events.
      mPopupWindow.setTouchInterceptor(new View.OnTouchListener() {
        @Override public boolean onTouch(View v, MotionEvent event) {

          final int x = (int) event.getX();
          final int y = (int) event.getY();

          if ((event.getAction() == MotionEvent.ACTION_DOWN) && ((x < 0)
              || (x >= mWidth)
              || (y < 0)
              || (y >= mHeight))) {
            //outside
            Log.d(TAG, "onTouch outside:mWidth=" + mWidth + ",mHeight=" + mHeight);
            return true;
          } else if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
            //outside
            Log.d(TAG, "onTouch outside event:mWidth=" + mWidth + ",mHeight=" + mHeight);
            return true;
          }
          return false;
        }
      });
    } else {
      mPopupWindow.setFocusable(mFocusable);
      mPopupWindow.setOutsideTouchable(mOutsideTouchable);
      mPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }
  }

  /****Custom lifecycle approach****/

  /**
   * PopupWindow object creation is complete
   */
  protected void onPopupWindowCreated() {
    //Execution settings PopupWindow property can also be set through the Builder
    //setContentView(x,x,x);
    //...
    initAttributes();
  }

  protected void onPopupWindowViewCreated(View contentView) {
    initViews(contentView, self());
  }

  protected void onPopupWindowDismiss() {
  }

  /**
   * You can set the properties required by PopupWindow in this method.
   */
  protected abstract void initAttributes();

  /**
   * Initialize view {@see getView()}
   */
  protected abstract void initViews(View view, T popup);

  /**
   * Do you need to measure the size of the contentView?
   * If you need to re-measure and assign a width or height
   * Note: The width and height of this method may not be accurate. MATCH_PARENT cannot obtain accurate width and height.
   */
  private void measureContentView() {
    final View contentView = getContentView();
    if (mWidth <= 0 || mHeight <= 0) {
      //Measurement size
      contentView.measure(0, View.MeasureSpec.UNSPECIFIED);
      if (mWidth <= 0) {
        mWidth = contentView.getMeasuredWidth();
      }
      if (mHeight <= 0) {
        mHeight = contentView.getMeasuredHeight();
      }
    }
  }

  /**
   * Register GlobalLayoutListener for accurate width and height
   */
  private void registerOnGlobalLayoutListener() {
    final View view = getContentView();
    view.getViewTreeObserver()
        .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
          @Override public void onGlobalLayout() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
              view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            } else {
              view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
            mWidth = getContentView().getWidth();
            mHeight = getContentView().getHeight();

            isRealWHAlready = true;
            isNeedReMeasureWH = false;

            if (mOnRealWHAlreadyListener != null) {
              mOnRealWHAlreadyListener.onRealWHAlready(BasePopup.this, mWidth, mHeight,
                  mAnchorView == null ? 0 : mAnchorView.getWidth(),
                  mAnchorView == null ? 0 : mAnchorView.getHeight());
            }
            //                Log.d(TAG, "onGlobalLayout finished. isShowing=" + isShowing());
            if (isShowing() && isAtAnchorViewMethod) {
              updateLocation(mWidth, mHeight, mAnchorView, mYGravity, mXGravity, mOffsetX,
                  mOffsetY);
            }
          }
        });
  }

  /**
   * Update PopupWindow to a precise location
   */
  private void updateLocation(int width, int height, @NonNull View anchor,
      @YGravity final int yGravity, @XGravity int xGravity, int x, int y) {
    if (mPopupWindow == null) {
      return;
    }
    x = calculateX(anchor, xGravity, width, x);
    y = calculateY(anchor, yGravity, height, y);
    mPopupWindow.update(anchor, x, y, width, height);
  }

  /****Set property method****/

  public T setContext(Context context) {
    this.mContext = context;
    return self();
  }

  public T setContentView(View contentView) {
    this.mContentView = contentView;
    this.mLayoutId = 0;
    return self();
  }

  public T setContentView(@LayoutRes int layoutId) {
    this.mContentView = null;
    this.mLayoutId = layoutId;
    return self();
  }

  public T setContentView(Context context, @LayoutRes int layoutId) {
    this.mContext = context;
    this.mContentView = null;
    this.mLayoutId = layoutId;
    return self();
  }

  public T setContentView(View contentView, int width, int height) {
    this.mContentView = contentView;
    this.mLayoutId = 0;
    this.mWidth = width;
    this.mHeight = height;
    return self();
  }

  public T setContentView(@LayoutRes int layoutId, int width, int height) {
    this.mContentView = null;
    this.mLayoutId = layoutId;
    this.mWidth = width;
    this.mHeight = height;
    return self();
  }

  public T setContentView(Context context, @LayoutRes int layoutId, int width, int height) {
    this.mContext = context;
    this.mContentView = null;
    this.mLayoutId = layoutId;
    this.mWidth = width;
    this.mHeight = height;
    return self();
  }

  public T setWidth(int width) {
    this.mWidth = width;
    return self();
  }

  public T setHeight(int height) {
    this.mHeight = height;
    return self();
  }

  public T setAnchorView(View view) {
    this.mAnchorView = view;
    return self();
  }

  public T setYGravity(@YGravity int yGravity) {
    this.mYGravity = yGravity;
    return self();
  }

  public T setXGravity(@XGravity int xGravity) {
    this.mXGravity = xGravity;
    return self();
  }

  public T setOffsetX(int offsetX) {
    this.mOffsetX = offsetX;
    return self();
  }

  public T setOffsetY(int offsetY) {
    this.mOffsetY = offsetY;
    return self();
  }

  public T setAnimationStyle(@StyleRes int animationStyle) {
    this.mAnimationStyle = animationStyle;
    return self();
  }

  public T setFocusable(boolean focusable) {
    this.mFocusable = focusable;
    return self();
  }

  public T setOutsideTouchable(boolean outsideTouchable) {
    this.mOutsideTouchable = outsideTouchable;
    return self();
  }

  /**
   * Is it possible to click outside the PopupWindow to dismiss
   */
  public T setFocusAndOutsideEnable(boolean focusAndOutsideEnable) {
    this.mFocusAndOutsideEnable = focusAndOutsideEnable;
    return self();
  }

  /**
   * Background darkening support api>=18
   */
  public T setBackgroundDimEnable(boolean isDim) {
    this.isBackgroundDim = isDim;
    return self();
  }

  public T setDimValue(@FloatRange(from = 0.0f, to = 1.0f) float dimValue) {
    this.mDimValue = dimValue;
    return self();
  }

  public T setDimColor(@ColorInt int color) {
    this.mDimColor = color;
    return self();
  }

  public T setDimView(@NonNull ViewGroup dimView) {
    this.mDimView = dimView;
    return self();
  }

  @RequiresApi(api = Build.VERSION_CODES.M)
  public T setEnterTransition(Transition enterTransition) {
    this.mEnterTransition = enterTransition;
    return self();
  }

  @RequiresApi(api = Build.VERSION_CODES.M) public T setExitTransition(Transition exitTransition) {
    this.mExitTransition = exitTransition;
    return self();
  }

  public T setInputMethodMode(int mode) {
    this.mInputMethodMode = mode;
    return self();
  }

  public T setSoftInputMode(int mode) {
    this.mSoftInputMode = mode;
    return self();
  }

  /**
   * Do you need to regain the width and height?
   */
  public T setNeedReMeasureWH(boolean needReMeasureWH) {
    this.isNeedReMeasureWH = needReMeasureWH;
    return self();
  }

  /**
   * Check if the apply() method is called
   *  
   *  @param isAtAnchorView is showAt
   */
  private void checkIsApply(boolean isAtAnchorView) {
    if (this.isAtAnchorViewMethod != isAtAnchorView) {
      this.isAtAnchorViewMethod = isAtAnchorView;
    }
    if (mPopupWindow == null) {
      apply();
    }
  }

  /**
   * To use this method, you need to call the property set {@see setAnchorView()} when setAnchorView() is created.
   */
  public void showAsDropDown() {
    if (mAnchorView == null) {
      return;
    }
    showAsDropDown(mAnchorView, mOffsetX, mOffsetY);
  }

  /**
   * PopupWindow's own display method
   */
  public void showAsDropDown(View anchor, int offsetX, int offsetY) {
    //Prevent forgetting to call the apply() method
    checkIsApply(false);

    handleBackgroundDim();
    mAnchorView = anchor;
    mOffsetX = offsetX;
    mOffsetY = offsetY;
    //Whether to regain the width and height
    if (isNeedReMeasureWH) {
      registerOnGlobalLayoutListener();
    }
    mPopupWindow.showAsDropDown(anchor, mOffsetX, mOffsetY);
  }

  public void showAsDropDown(View anchor) {
    //Prevent forgetting to call the apply() method
    checkIsApply(false);

    handleBackgroundDim();
    mAnchorView = anchor;
    //Whether to regain the width and height
    if (isNeedReMeasureWH) {
      registerOnGlobalLayoutListener();
    }
    mPopupWindow.showAsDropDown(anchor);
  }

  @RequiresApi(api = Build.VERSION_CODES.KITKAT)
  public void showAsDropDown(View anchor, int offsetX, int offsetY, int gravity) {
    //Prevent forgetting to call the apply() method
    checkIsApply(false);

    handleBackgroundDim();
    mAnchorView = anchor;
    mOffsetX = offsetX;
    mOffsetY = offsetY;
    //Whether to regain the width and height this
    if (isNeedReMeasureWH) {
      registerOnGlobalLayoutListener();
    }
    PopupWindowCompat.showAsDropDown(mPopupWindow, anchor, mOffsetX, mOffsetY, gravity);
  }

  public void showAtLocation(View parent, int gravity, int offsetX, int offsetY) {
    //Prevent forgetting to call the apply() method
    checkIsApply(false);

    handleBackgroundDim();
    mAnchorView = parent;
    mOffsetX = offsetX;
    mOffsetY = offsetY;
    //Whether to regain the width and height
    if (isNeedReMeasureWH) {
      registerOnGlobalLayoutListener();
    }
    mPopupWindow.showAtLocation(parent, gravity, mOffsetX, mOffsetY);
  }

  /**
   * Relative anchor view display
   * <p>
   * Use this method to call the property set {@see setAnchorView()} at the time of creation.
   * <p>
   * Note: If using VerticalGravity and HorizontalGravity, make sure PopupWindow does not exceed the screen boundary after use.
   * If the screen boundary is exceeded, VerticalGravity and HorizontalGravity may not work, and you will not achieve the effect you want.
   */
  public void showAtAnchorView() {
    if (mAnchorView == null) {
      return;
    }
    showAtAnchorView(mAnchorView, mYGravity, mXGravity);
  }

  /**
   * Relative anchor view display, applicable width and height are not match_parent
   * <p>
   * Note: If using VerticalGravity and HorizontalGravity, make sure PopupWindow does not exceed the screen boundary after use.
   * If the screen boundary is exceeded, VerticalGravity and HorizontalGravity may not work, and you will not achieve the effect you want. *
   */
  public void showAtAnchorView(@NonNull View anchor, @YGravity int vertGravity,
      @XGravity int horizGravity) {
    showAtAnchorView(anchor, vertGravity, horizGravity, 0, 0);
  }

  /**
   * Relative anchor view display, applicable width and height are not match_parent
   * <p>
   * Note: If using VerticalGravity and HorizontalGravity, make sure PopupWindow does not exceed the screen boundary after use.
   * If the screen boundary is exceeded, VerticalGravity and HorizontalGravity may not work, and you will not achieve the effect you want.
   *
   * @param vertGravity alignment in the vertical direction
   * @param horizGravity Horizontal alignment
   * @param x horizontal offset
   * @param y vertical offset
   */
  public void showAtAnchorView(@NonNull View anchor, @YGravity final int vertGravity,
      @XGravity int horizGravity, int x, int y) {
    //Prevent forgetting to call the apply() method
    checkIsApply(true);

    mAnchorView = anchor;
    mOffsetX = x;
    mOffsetY = y;
    mYGravity = vertGravity;
    mXGravity = horizGravity;
    //Processing background dimming
    handleBackgroundDim();
    x = calculateX(anchor, horizGravity, mWidth, mOffsetX);
    y = calculateY(anchor, vertGravity, mHeight, mOffsetY);
    //Whether to regain the width and height
    if (isNeedReMeasureWH) {
      registerOnGlobalLayoutListener();
    }
    //        Log.i(TAG, "showAtAnchorView: w=" + measuredW + ",y=" + measuredH);
    PopupWindowCompat.showAsDropDown(mPopupWindow, anchor, x, y, Gravity.NO_GRAVITY);
  }

  /**
   * Calculate y offset based on vertical gravity
   */
  private int calculateY(View anchor, int vertGravity, int measuredH, int y) {
    switch (vertGravity) {
      case YGravity.ABOVE:
        //Above the anchor view
        y -= measuredH + anchor.getHeight();
        break;
      case YGravity.ALIGN_BOTTOM:
        //Anchor view bottom alignment
        y -= measuredH;
        break;
      case YGravity.CENTER:
        //Anchor view centered vertically
        y -= anchor.getHeight() / 2 + measuredH / 2;
        break;
      case YGravity.ALIGN_TOP:
        //Anchor view top alignment
        y -= anchor.getHeight();
        break;
      case YGravity.BELOW:
        //Under anchor view
        // Default position.
        break;
    }

    return y;
  }

  /**
   * Calculate x offset based on horizontal gravity
   */
  private int calculateX(View anchor, int horizGravity, int measuredW, int x) {
    switch (horizGravity) {
      case XGravity.LEFT:
        //Anchor view left
        x -= measuredW;
        break;
      case XGravity.ALIGN_RIGHT:
        //Align with the right side of the anchor view
        x -= measuredW - anchor.getWidth();
        break;
      case XGravity.CENTER:
        //Anchor view horizontally centered
        x += anchor.getWidth() / 2 - measuredW / 2;
        break;
      case XGravity.ALIGN_LEFT:
        //Align with the left side of the anchor view
        // Default position.
        break;
      case XGravity.RIGHT:
        //Anchor view right
        x += anchor.getWidth();
        break;
    }

    return x;
  }

  public T setOnDismissListener(PopupWindow.OnDismissListener listener) {
    this.mOnDismissListener = listener;
    return self();
  }

  public T setOnRealWHAlreadyListener(OnRealWHAlreadyListener listener) {
    this.mOnRealWHAlreadyListener = listener;
    return self();
  }

  /**
   * Processing background dimming
   * https://blog.nex3z.com/2016/12/04/%E5%BC%B9%E5%87%BApopupwindow%E5%90%8E%E8%AE%A9%E8%83%8C%E6%99%AF%E5%8F%98%E6%9A%97%E7%9A%84%E6%96%B9%E6%B3%95/
   */
  private void handleBackgroundDim() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
      if (!isBackgroundDim) {
        return;
      }
      if (mDimView != null) {
        applyDim(mDimView);
      } else {
        if (getContentView() != null
            && getContentView().getContext() != null
            && getContentView().getContext() instanceof Activity) {
          Activity activity = (Activity) getContentView().getContext();
          applyDim(activity);
        }
      }
    }
  }

  @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2) private void applyDim(Activity activity) {
    ViewGroup parent = (ViewGroup) activity.getWindow().getDecorView().getRootView();
    //Activity and layout
    //        ViewGroup parent = (ViewGroup) parent1.getChildAt(0);
    Drawable dimDrawable = new ColorDrawable(mDimColor);
    dimDrawable.setBounds(0, 0, parent.getWidth(), parent.getHeight());
    dimDrawable.setAlpha((int) (255 * mDimValue));
    ViewGroupOverlay overlay = parent.getOverlay();
    overlay.add(dimDrawable);
  }

  @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2) private void applyDim(ViewGroup dimView) {
    Drawable dimDrawable = new ColorDrawable(mDimColor);
    dimDrawable.setBounds(0, 0, dimView.getWidth(), dimView.getHeight());
    dimDrawable.setAlpha((int) (255 * mDimValue));
    ViewGroupOverlay overlay = dimView.getOverlay();
    overlay.add(dimDrawable);
  }

  /**
   * Clear background darkens
   */
  private void clearBackgroundDim() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
      if (isBackgroundDim) {
        if (mDimView != null) {
          clearDim(mDimView);
        } else {
          if (getContentView() != null) {
            Activity activity = (Activity) getContentView().getContext();
            if (activity != null) {
              clearDim(activity);
            }
          }
        }
      }
    }
  }

  @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2) private void clearDim(Activity activity) {
    ViewGroup parent = (ViewGroup) activity.getWindow().getDecorView().getRootView();
    //activity跟布局
    //        ViewGroup parent = (ViewGroup) parent1.getChildAt(0);
    ViewGroupOverlay overlay = parent.getOverlay();
    overlay.clear();
  }

  @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2) private void clearDim(ViewGroup dimView) {
    ViewGroupOverlay overlay = dimView.getOverlay();
    overlay.clear();
  }

  public View getContentView() {
    if (mPopupWindow != null) {
      return mPopupWindow.getContentView();
    } else {
      return null;
    }
  }

  public PopupWindow getPopupWindow() {
    return mPopupWindow;
  }

  public int getWidth() {
    return mWidth;
  }

  public int getHeight() {
    return mHeight;
  }

  public int getXGravity() {
    return mXGravity;
  }

  public int getYGravity() {
    return mYGravity;
  }

  /**
   * Get the offset in the x-axis direction
   */
  public int getOffsetX() {
    return mOffsetX;
  }

  /**
   * Get the offset in the y-axis direction
   */
  public int getOffsetY() {
    return mOffsetY;
  }

  public boolean isShowing() {
    return mPopupWindow != null && mPopupWindow.isShowing();
  }

  /**
   * Whether the accuracy of the width and height is achieved
   */
  public boolean isRealWHAlready() {
    return isRealWHAlready;
  }

  public <T extends View> T findViewById(@IdRes int viewId) {
    View view = null;
    if (getContentView() != null) {
      view = getContentView().findViewById(viewId);
    }
    return (T) view;
  }

  public void dismiss() {
    if (mPopupWindow != null) {
      mPopupWindow.dismiss();
    }
  }

  @Override public void onDismiss() {
    handleDismiss();
  }

  /**
   * Processing some logic after PopupWindow disappears
   */
  private void handleDismiss() {
    if (mOnDismissListener != null) {
      mOnDismissListener.onDismiss();
    }

    //Clear background darkens
    clearBackgroundDim();
    if (mPopupWindow != null && mPopupWindow.isShowing()) {
      mPopupWindow.dismiss();
    }
    onPopupWindowDismiss();
  }

  /**
   * Whether PopupWindow is displayed in the window
   * Used to get accurate PopupWindow width and height, you can reset the offset
   */
  public interface OnRealWHAlreadyListener {

    /**
     * Execute before update method after show method
     *
     * @param popWidth PopupWindow accurate width
     * @param popHeight PopupWindow is accurate and high
     * @param anchorW anchor view width
     * @param anchorH Anchor View High
     */
    void onRealWHAlready(BasePopup basePopup, int popWidth, int popHeight, int anchorW,
        int anchorH);
  }
}
